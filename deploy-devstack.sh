#!/bin/bash -x

sudo apt update
sudo apt install -y python-dev libssl-dev libxml2-dev curl \
                    libmysqlclient-dev libxslt-dev libpq-dev git \
                    libffi-dev gettext build-essential python3-dev \
                    python-pip

sudo pip install virtualenv flake8 tox testrepository git-review ansible

strigazi_home=/home/strigazi
ssh-keygen -t rsa -N '' -f ${strigazi_home}/.ssh/id_rsa
chmod 400 ${strigazi_home}/.ssh/id_rsa
chmod 400 ${strigazi_home}/.ssh/id_rsa.pub
# Add localost to know_hosts
ssh-keyscan 127.0.0.1 > ${strigazi_home}/.ssh/known_hosts
# ssh configguration file, to be specified with ssh -F
cat > ${strigazi_home}/.ssh/config <<EOF
Host localhost
     HostName 127.0.0.1
     IdentityFile ${strigazi_home}/.ssh/id_rsa
     UserKnownHostsFile ${strigazi_home}/.ssh/known_hosts
EOF

git clone https://git.openstack.org/openstack-dev/devstack
sudo devstack/tools/create-stack-user.sh
sudo cp -r devstack /opt/stack
sudo chown -R stack:stack /opt/stack/devstack

sudo su -s /bin/sh -c "mkdir -p /opt/stack/.ssh" stack
sudo su -s /bin/sh -c "chmod 700 /opt/stack/.ssh" stack

sudo su -s /bin/sh -c "mkdir -p /opt/stack/.ssh" stack
sudo su -s /bin/sh -c "chmod 700 /opt/stack/.ssh" stack
# Add the public to the host authorized_keys file.
sudo su -s /bin/sh -c "mkdir -p /opt/stack/.ssh" stack
sudo cat ${strigazi_home}/.ssh/id_rsa.pub > /opt/stack/.ssh/authorized_keys
sudo su -s /bin/sh -c "chmod 600 /opt/stack/.ssh/authorized_keys" stack
sudo su -s /bin/sh -c "cat >> /opt/stack/.ssh/authorized_keys << END
ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAlKi4GIqp9BG9buHOziaL0TeW7bHrd+nVkhuW8BNQC5H4VPj2AJXq2d/2UrRRrdLFiQTEKuPFWhwYw3GzTYq8fZTMVjU7buIbqKx5xCa//k7htNIwNRj5ao/6B3sBubQUI+jeDmDil/sRgL9lfC77eqVFr5G7SGmKWBm+3rGYkPu6M1xFmeodpLeuwIhBj2rfiP+cU6nacnpIgp/FjVSilmEW94yuf1g7RwkeYlu6nEas1SHXI0Lf50lEwiY322prlrhzgOTnEo6cpFwZjM4L+ih1enLAhHRc5QsfArP3M2Jn9LEKy5t9BLgIHfEimTTM7eTjHm9t2hSoUTT6Ts/w0Q== strigazi@lxplus0002.cern.ch
END" stack
STRIGAZI_PUBLIC_KEY=$(cat ${strigazi_home}/.ssh/id_rsa.pub)
sudo su -s /bin/sh -c "cat >> /opt/stack/.ssh/authorized_keys << END
${STRIGAZI_PUBLIC_KEY}
END" stack


cat > ${strigazi_home}/local.conf <<END
[[local|localrc]]

IP_VERSION=4
SERVICE_IP_VERSION=4
PUBLIC_INTERFACE=em1

DATABASE_PASSWORD=password
RABBIT_PASSWORD=password
SERVICE_TOKEN=password
SERVICE_PASSWORD=password
ADMIN_PASSWORD=password

enable_service tls-proxy

enable_plugin barbican https://git.openstack.org/openstack/barbican

enable_plugin neutron-lbaas https://git.openstack.org/openstack/neutron-lbaas
enable_plugin octavia https://git.openstack.org/openstack/octavia
disable_service q-lbaas
enable_service q-lbaasv2
enable_service octavia
enable_service o-cw
enable_service o-hk
enable_service o-hm
enable_service o-api

disable_service horizon

enable_plugin heat https://git.openstack.org/openstack/heat
enable_plugin magnum https://git.openstack.org/openstack/magnum

ENABLED_SERVICES+=,octavia,o-cw,o-hk,o-hm,o-api
ENABLED_SERVICES+=,q-svc,q-agt,q-dhcp,q-l3,q-meta
ENABLED_SERVICES+=,q-lbaasv2


VOLUME_BACKING_FILE_SIZE=20G
END

sudo cp ${strigazi_home}/local.conf /opt/stack/devstack/local.conf
sudo chown stack:stack /opt/stack/devstack/local.conf
